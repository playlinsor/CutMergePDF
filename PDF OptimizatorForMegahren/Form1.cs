﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace PDF_OptimizatorForMegahren
{
    public partial class Form1 : Form
    {
        int LabelNumber = 1;
        const int BlinkCount = 7;
        int CurrentBlink = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadFiles();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.SelectionMode = SelectionMode.MultiSimple;
            listBox1.ClearSelected();

            label5.Text = button4.Text;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MergeFile();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectionMode == SelectionMode.One)
            {
                CutPDF();
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            } else MessageBox.Show("Выбери позязя режим для разрезания");
        }


        #region Функции

        public void MergeFile()
        {
            try
            {
                if (listBox1.SelectionMode == SelectionMode.MultiSimple)
                {
                    string[] ListMergeFile = new string[listBox1.SelectedItems.Count];
                    int idx = 0;

                    foreach (var item in listBox1.SelectedItems)
                    {
                        ListMergeFile[idx++] = item.ToString();
                    }

                    string DirectoryOutput = Path.GetDirectoryName(ListMergeFile[0]) + @"\!Обработанные файлы - обьединение\";
                    if (!Directory.Exists(DirectoryOutput)) Directory.CreateDirectory(DirectoryOutput);


                    workPDF.CombineMultiplePDFs(ListMergeFile, DirectoryOutput + "\\" + Path.GetFileName(ListMergeFile[0]));

                    StartBlink(2);
                    labelInfo4.Text = "Склеяно - " + idx;

                    for (int i = listBox1.SelectedIndices.Count - 1; i >= 0; i--)
                    {
                        listBox1.Items.RemoveAt(listBox1.SelectedIndices[i]);
                    }

                }
                else MessageBox.Show("Выбери позязя режим для обьединения");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LoadFiles()
        {
            try
            {
                string[] AllFiles = Directory.GetFiles(textBox1.Text, "*.pdf");
                listBox1.Items.Clear();
                StartBlink(1);
                labelInfo3.Text = "Загружено - " + AllFiles.Length;

                foreach (string item in AllFiles)
                {
                    listBox1.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CutPDF()
        {
            
            try
            {
                string path = listBox1.Items[listBox1.SelectedIndex].ToString();
                string DirectoryOutput = Path.GetDirectoryName(path) + @"\!Обработанные файлы - разрезание\";

                if (!Directory.Exists(DirectoryOutput)) Directory.CreateDirectory(DirectoryOutput);
                else
                {
                    if (checkBox2.Checked)
                    {
                        Directory.Delete(DirectoryOutput, true);
                        Directory.CreateDirectory(DirectoryOutput);
                    }
                }
                StartBlink(2);
                labelInfo4.Text ="Обработано - "+workPDF.SplitPdf(path, DirectoryOutput, GetCurrentPatternOnForm());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public workPDF.PattertnCutPDF GetCurrentPatternOnForm()
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: return workPDF.PattertnCutPDF.OnePage;
                case 1: return workPDF.PattertnCutPDF.TwoPage;
                case 2: return workPDF.PattertnCutPDF.ThreePage;
                case 3: return workPDF.PattertnCutPDF.FourPage;
                case 5: return workPDF.PattertnCutPDF.TwoOnePage;
                case 6: return workPDF.PattertnCutPDF.TreeOnePage;
                case 8: return workPDF.PattertnCutPDF.TreeTwoOnePage;
                default:return workPDF.PattertnCutPDF.OnePage;
  
            }
           
        }

        #endregion

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.ClearSelected();
            listBox1.SelectionMode = SelectionMode.One;
            label5.Text = button5.Text;
        }


        private void BlinkCurrentLabel(ref Label BlinkLabel1)
        {
            CurrentBlink++;
            if (CurrentBlink >= BlinkCount)
            {
                BlinkLabel1.BackColor = Form1.DefaultBackColor;
                BlinkLabel1.ForeColor = Form1.DefaultForeColor;
                CurrentBlink = 0;
                BlinkTimer.Enabled = false;
            }
            else
            {
                if (BlinkLabel1.BackColor != Color.ForestGreen)
                {
                    BlinkLabel1.BackColor = Color.ForestGreen;
                    BlinkLabel1.ForeColor = Color.Yellow;
                }
                else
                {
                    BlinkLabel1.BackColor = Form1.DefaultBackColor;
                    BlinkLabel1.ForeColor = Form1.DefaultForeColor;
                }
            }
        }

        private void StartBlink(int label)
        {
             LabelNumber = label;
            BlinkTimer.Enabled = true;
        }

        private void BlinkTimer_Tick(object sender, EventArgs e)
        {
            switch (LabelNumber)
            {
                case 1:
                    BlinkCurrentLabel(ref labelInfo3);
                    break;
                case 2:
                    BlinkCurrentLabel(ref labelInfo4);
                    break;
                default:
                    break;
            }
        }
    }
}
