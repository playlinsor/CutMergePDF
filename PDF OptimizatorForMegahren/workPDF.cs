﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace PDF_OptimizatorForMegahren
{
    public class workPDF
    {
        public enum PattertnCutPDF {OnePage=1,TwoPage=2,ThreePage=3,FourPage=5,TwoOnePage=21,TreeOnePage=31,TreeTwoOnePage=321 };



        public static void CombineMultiplePDFs(string[] fileNames, string outFile)
        {
            // step 1: creation of a document-object
            Document document = new Document();

            // step 2: we create a writer that listens to the document
            PdfCopy writer = new PdfCopy(document, new FileStream(outFile, FileMode.Create));
            if (writer == null)
            {
                return;
            }

            // step 3: we open the document
            document.Open();

            foreach (string fileName in fileNames)
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(fileName);
                reader.ConsolidateNamedDestinations();

                // step 4: we add content
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage page = writer.GetImportedPage(reader, i);
                    writer.AddPage(page);
                }

                PRAcroForm form = reader.AcroForm;
                if (form != null)
                {
                    writer.AddDocument(reader);
                    
                }

                reader.Close();
            }

            // step 5: we close the document and writer
            writer.Close();
            document.Close();
        }

        public static int SplitPdf(string Source,string destination,PattertnCutPDF Pattern)
        {
            int current = 0;
            switch (Pattern)
            {
                case PattertnCutPDF.OnePage:
                    current = CutEveryNumberPage(Source, destination, 1);
                    break;
                case PattertnCutPDF.TwoPage:
                    current = CutEveryNumberPage(Source, destination, 2);
                    break;
                case PattertnCutPDF.ThreePage:
                    current = CutEveryNumberPage(Source, destination, 3);
                    break;
                case PattertnCutPDF.FourPage:
                    current = CutEveryNumberPage(Source, destination, 4);
                    break;
                case PattertnCutPDF.TwoOnePage:
                    current = CutIntervalPage(Source, destination, 2, 1);
                    break;
                case PattertnCutPDF.TreeOnePage:
                    current = CutIntervalPage(Source, destination, 3, 1);
                    break;
                case PattertnCutPDF.TreeTwoOnePage:
                    current = CutIntervalTREEPage(Source, destination, 3, 2,1);
                    break;
                default:
                    break;
            }

            return current;
        }


        // TODO: Можно оптимизировать!
        static private int CutIntervalPage(string Source, string destination, int OneInterval, int Twointerval)
        {
            FileInfo fi = new FileInfo(Source);
            PdfReader reader = new PdfReader(Source);

            int CountCut = reader.NumberOfPages / (OneInterval + Twointerval);
            int idx = 0;


            int StartPageCut1 = 1;
            int EndPageCut1 = OneInterval;

            int StartPageCut2 = OneInterval + 1;
            int EndPageCut2 = StartPageCut2 + Twointerval - 1;


            while (idx < CountCut)
            {

                CreatePdf(destination + string.Format("\\{0}_{1}_1.pdf", Path.GetFileNameWithoutExtension(fi.FullName), idx + 1), reader, StartPageCut1, EndPageCut1);

                if (EndPageCut1 != reader.NumberOfPages)
                {
                    CreatePdf(destination + string.Format("\\{0}_{1}_2.pdf", Path.GetFileNameWithoutExtension(fi.FullName), idx + 1), reader, StartPageCut2, EndPageCut2);
                }

                StartPageCut1 = EndPageCut2 + 1;
                EndPageCut1 = EndPageCut2 + OneInterval;
                if (EndPageCut1 > reader.NumberOfPages) EndPageCut1 = reader.NumberOfPages;


                if (EndPageCut1 != reader.NumberOfPages)
                {
                    StartPageCut2 = EndPageCut1 + 1;
                    EndPageCut2 = StartPageCut2 + Twointerval -1;
                    if (EndPageCut2 > reader.NumberOfPages) EndPageCut2 = reader.NumberOfPages;
                }

                idx++;
            }

            return idx;


        }

        static private int CutEveryNumberPage(string Source,string destination,int NumberPageInOneDocument)
        {
            FileInfo fi = new FileInfo(Source);
            PdfReader reader = new PdfReader(Source);

            int CountCut = reader.NumberOfPages / NumberPageInOneDocument;
            int idx = 0;
            
            while (idx < CountCut)
            {
                int StartPageCut = idx * NumberPageInOneDocument+1;
                int EndPageCut = idx * NumberPageInOneDocument + NumberPageInOneDocument;
                if (EndPageCut > reader.NumberOfPages) EndPageCut = reader.NumberOfPages;

                CreatePdf(destination+string.Format("\\{0}_{1}.pdf",Path.GetFileNameWithoutExtension(fi.FullName), idx+1),reader, StartPageCut, EndPageCut);

                idx++;
            }

            return idx;

        }

        static private int CutIntervalTREEPage(string Source, string destination, int OneInterval, int Twointerval,int Treeinterval)
        {
            FileInfo fi = new FileInfo(Source);
            PdfReader reader = new PdfReader(Source);

            int CountCut = reader.NumberOfPages / (OneInterval + Twointerval+Treeinterval) + 1;
            int idx = 0;


            int StartPageCut1 = 1;
            int EndPageCut1 = OneInterval;

            int StartPageCut2 = OneInterval + 1;
            int EndPageCut2 = StartPageCut2 + Twointerval - 1;

            int StartPageCut3 = EndPageCut2 + 1;
            int EndPageCut3 = StartPageCut3 + Treeinterval - 1;


            while (idx < CountCut)
            {

                CreatePdf(destination + string.Format("\\{0}_{1}_1.pdf", Path.GetFileNameWithoutExtension(fi.FullName), idx + 1), reader, StartPageCut1, EndPageCut1);

                if (EndPageCut1 != reader.NumberOfPages)
                {
                    CreatePdf(destination + string.Format("\\{0}_{1}_2.pdf", Path.GetFileNameWithoutExtension(fi.FullName), idx + 1), reader, StartPageCut2, EndPageCut2);
                }

                if (EndPageCut2 != reader.NumberOfPages)
                {
                    CreatePdf(destination + string.Format("\\{0}_{1}_3.pdf", Path.GetFileNameWithoutExtension(fi.FullName), idx + 1), reader, StartPageCut3, EndPageCut3);
                }

                StartPageCut1 = EndPageCut3 + 1;
                EndPageCut1 = EndPageCut3 + OneInterval;
                if (EndPageCut1 > reader.NumberOfPages) EndPageCut1 = reader.NumberOfPages;


                if (EndPageCut1 != reader.NumberOfPages)
                {
                    StartPageCut2 = EndPageCut1 + 1;
                    EndPageCut2 = StartPageCut2 + Twointerval - 1;
                    if (EndPageCut2 > reader.NumberOfPages) EndPageCut2 = reader.NumberOfPages;
                }

                if (EndPageCut2 != reader.NumberOfPages)
                {
                    StartPageCut3 = EndPageCut2 + 1;
                    EndPageCut3 = StartPageCut3 + Treeinterval - 1;
                    if (EndPageCut3 > reader.NumberOfPages) EndPageCut3 = reader.NumberOfPages;
                }




                idx++;
            }

            return idx;


        }


        /// <summary>
        /// Создаем Новый ПДФ Файл
        /// </summary>
        /// <param name="PathPdf">Путь к файлу</param>
        /// <param name="MainReader">Откуда брать инфу</param>
        /// <param name="pageStart">Начальная страница</param>
        /// <param name="pageEnd">Конечная страница</param>
        static public void CreatePdf(string PathPdf, PdfReader MainReader, int pageStart, int pageEnd)
        {
            if (pageEnd <= MainReader.NumberOfPages)
            {
                // Создание нового файла
                FileStream fs = File.Create(PathPdf);
                Document document = new Document(MainReader.GetPageSizeWithRotation(1));
                PdfCopy copy = new PdfCopy(document, fs);
                document.Open();

                // Копирование страниц
                PdfImportedPage page;
                for (int i = pageStart; i <= pageEnd; i++)
                {
                    page = copy.GetImportedPage(MainReader, i);
                    copy.AddPage(page);
                }

                copy.Close();
                document.Close();
                fs.Close();


            }
        }
    }
}
